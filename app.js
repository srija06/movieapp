const express=require("express");
const app=express();
const request=require("request");
// const axios=require("axios")

app.use(express.urlencoded({extended:true}));



const path=require('path');

app.set("view engine","ejs");

app.set("views",path.join(__dirname,'views'));
const PORT=4000;
app.get("/",(req,res)=>{
    res.render("Movies/home")
})

app.get("/topMovies",(req,res)=>{

const options = {
  method: 'GET',
  url: 'https://imdb-top-100-movies.p.rapidapi.com/premiummovies',
  headers: {
    'X-RapidAPI-Key': '842d8fea45mshf9228c05da8dc9ap19036djsn6357134594ff',
    'X-RapidAPI-Host': 'imdb-top-100-movies.p.rapidapi.com'
  }
};
request(options,function (err,response,body) {
    if(response.statusCode==500){
        console.log("Error Invalid Request")
    }else if(!err&&response.statusCode!=500&&response){
    
        const resp=JSON.parse(response.body);
    
        const topMovies=resp;
        
        res.render("Movies/topMovies",{topMovies})
    }
})
});

app.post("/search",async(req,res)=>{
    const movieName=req.body.SearchMovie;
    console.log("Movie Name :",movieName)
    const movieUrl=`http://www.omdbapi.com/?s=${movieName}&apikey=thewdb`
    request(movieUrl,function(err,response,body){
        if(response.statusCode==500){
            console.log("No Movie Found")
        }else if(!err&&response.statusCode!=500&&response){
            const movieData=JSON.parse(body);
            console.log("Movie Data :",movieData)
            res.render("Movies/result",{movies:movieData});
        }
    })
  
});








app.listen(PORT,function(){
    console.log(`Server starting on port :${PORT}`)
})